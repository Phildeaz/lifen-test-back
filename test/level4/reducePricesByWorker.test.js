process.env.NODE_ENV = 'test'
const reducePricesByWorker = require('../../src/level4/reducePricesByWorker.js')
const mockedData = require('../../src/level4/data.json')

describe('#reducePricesByWorker', () => {
  test('should return expected result', () => {
    const expectedOutput = {
      workers: [
        { id: 1, price: 540 },
        { id: 2, price: 810 },
        { id: 3, price: 126 },
        { id: 4, price: 810 },
        { id: 5, price: 1920 },
      ],
      commission: {
        pdg_fee: 450.3,
        interim_shifts: 3,
      },
    }
    expect(reducePricesByWorker(mockedData)).toEqual(expectedOutput)
  })
  test('should return expected result with unknown interim worker.id', () => {
    const alteredMockedData = Object.assign({}, mockedData)
    alteredMockedData.workers.pop()

    const expectedOutput = {
      workers: [
        { id: 1, price: 540 },
        { id: 2, price: 810 },
        { id: 3, price: 126 },
        { id: 4, price: 810 },
      ],
      commission: {
        pdg_fee: 114.3,
        interim_shifts: 0,
      },
    }
    expect(reducePricesByWorker(alteredMockedData)).toEqual(expectedOutput)
  })
})
