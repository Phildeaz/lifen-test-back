process.env.NODE_ENV = 'test'
const reducePricesByWorkerStatus = require('../../src/level2/reducePricesByWorkerStatus.js')
const mockedData = require('../../src/level2/data.json')

describe('#reducePricesByWorkerStatus', () => {
  test('should return expected result', () => {
    const expectedOutput = {
      workers: [
        { id: 1, price: 810 },
        { id: 2, price: 810 },
        { id: 3, price: 252 },
        { id: 4, price: 540 },
      ],
    }
    expect(reducePricesByWorkerStatus(mockedData)).toEqual(expectedOutput)
  })
  test('should return expected result with unknown worker.id', () => {
    const alteredMockedData = Object.assign({}, mockedData)
    alteredMockedData.workers.pop()

    const expectedOutput = {
      workers: [
        { id: 1, price: 810 },
        { id: 2, price: 810 },
        { id: 3, price: 252 },
      ],
    }
    expect(reducePricesByWorkerStatus(alteredMockedData)).toEqual(
      expectedOutput
    )
  })
})
