process.env.NODE_ENV = 'test'
const reducePricesByWorker = require('../../src/level1/reducePricesByWorker.js')
const mockedData = require('../../src/level1/data.json')

describe('#reducePricesByWorker', () => {
  test('should return expected result', () => {
    const expectedOutput = {
      workers: [
        { id: 1, price: 690 },
        { id: 2, price: 300 },
        { id: 3, price: 460 },
        { id: 4, price: 200 },
      ],
    }
    expect(reducePricesByWorker(mockedData)).toEqual(expectedOutput)
  })
  test('should return expected result with unknown worker.id', () => {
    const alteredMockedData = Object.assign({}, mockedData)
    alteredMockedData.workers.pop()

    const expectedOutput = {
      workers: [
        { id: 1, price: 690 },
        { id: 2, price: 300 },
        { id: 3, price: 460 },
      ],
    }
    expect(reducePricesByWorker(alteredMockedData)).toEqual(expectedOutput)
  })
})
