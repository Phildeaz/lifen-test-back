process.env.NODE_ENV = 'test'
const reducePricesByWorker = require('../../src/level3/reducePricesByWorker.js')
const mockedData = require('../../src/level3/data.json')

describe('#reducePricesByWorker', () => {
  test('should return expected result', () => {
    const expectedOutput = {
      workers: [
        { id: 1, price: 270 },
        { id: 2, price: 810 },
        { id: 3, price: 630 },
        { id: 4, price: 810 },
      ],
    }
    expect(reducePricesByWorker(mockedData)).toEqual(expectedOutput)
  })
  test('should return expected result with unknown worker.id', () => {
    const alteredMockedData = Object.assign({}, mockedData)
    alteredMockedData.workers.pop()

    const expectedOutput = {
      workers: [
        { id: 1, price: 270 },
        { id: 2, price: 810 },
        { id: 3, price: 630 },
      ],
    }
    expect(reducePricesByWorker(alteredMockedData)).toEqual(expectedOutput)
  })
})
