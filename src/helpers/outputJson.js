'use strict'
const fse = require('fs-extra')
const path = require('path')

async function outputJson({ inputData, levelIndex }) {
  try {
    const outputFile = path.resolve(
      __dirname,
      `../../output/level${levelIndex}/output.json`
    )

    await fse.outputJson(outputFile, inputData, { spaces: 2 })

    console.log(`Successfully wrote to ~/output/level${levelIndex}/output.json`)
    console.log(await fse.readJson(outputFile))
  } catch (err) {
    console.error('Error while creating output.json', err)
  }
}

module.exports = outputJson
