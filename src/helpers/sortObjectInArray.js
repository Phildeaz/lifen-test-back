function sortObjectInArray({ data, key }) {
  return data.sort((a, b) => a[key] - b[key])
}

module.exports = sortObjectInArray
