function isWeekendDate(date) {
  let day
  if (typeof date.getDay === 'function') day = date.getDay()
  else if (typeof date === 'string') {
    const d = new Date(date)
    day = d.getDay()
  } else {
    console.error('#isWeekendDate has bad param')
    return null
  }

  // 0 -> sunday, 6 -> saturday
  return day === 0 || day === 6
}

module.exports = isWeekendDate
