'use strict'

const isWeekendDate = require('../helpers/isWeekendDate')
const sortObjectInArray = require('../helpers/sortObjectInArray')

const priceByStatus = {
  medic: 270,
  intern: 126,
}

function reducePricesByWorker(data) {
  const result = data.shifts.reduce((acc, currValue) => {
    const userId = currValue.user_id
    const worker = data.workers.find(w => w.id === userId)

    // ignore unknown worker.id
    if (!worker) return acc

    let workerPricePerShift = priceByStatus[worker.status]
    if (isWeekendDate(currValue.start_date)) {
      workerPricePerShift = 2 * workerPricePerShift
    }

    const existingWorkerIndex = acc.findIndex(el => userId === el.id)
    if (existingWorkerIndex === -1) {
      acc.push({ id: userId, price: workerPricePerShift })
    } else {
      const currPrice = acc[existingWorkerIndex].price
      acc[existingWorkerIndex].price = currPrice + workerPricePerShift
    }
    return acc
  }, [])
  return { workers: sortObjectInArray({ data: result, key: 'id' }) }
}

module.exports = reducePricesByWorker
