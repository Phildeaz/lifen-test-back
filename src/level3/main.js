#!/usr/bin/env node

'use strict'
const mockedData = require('./data.json')
const outputJson = require('../helpers/outputJson.js')
const reducePricesByWorker = require('./reducePricesByWorker.js')

const inputData = reducePricesByWorker(mockedData)

outputJson({ inputData, levelIndex: 3 })
