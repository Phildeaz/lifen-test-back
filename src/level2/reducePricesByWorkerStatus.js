'use strict'

const priceByStatus = {
  medic: 270,
  intern: 126,
}

function reducePricesByWorkerStatus(data) {
  const result = data.shifts.reduce((acc, currValue) => {
    const userId = currValue.user_id
    const worker = data.workers.find(w => w.id === userId)

    // catch error with unknown worker.id
    if (!worker) return acc

    const workerPricePerShift = priceByStatus[worker.status]
    const existingWorkerIndex = acc.findIndex(el => userId === el.id)
    if (existingWorkerIndex === -1) {
      acc.push({ id: userId, price: workerPricePerShift })
    } else {
      const currPrice = acc[existingWorkerIndex].price
      acc[existingWorkerIndex].price = currPrice + workerPricePerShift
    }
    return acc
  }, [])
  return { workers: result }
}

module.exports = reducePricesByWorkerStatus
