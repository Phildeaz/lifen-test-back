#!/usr/bin/env node

'use strict'
const mockedData = require('./data.json')
const outputJson = require('../helpers/outputJson.js')
const reducePricesByWorkerStatus = require('./reducePricesByWorkerStatus.js')

const inputData = reducePricesByWorkerStatus(mockedData)

outputJson({ inputData, levelIndex: 2 })
