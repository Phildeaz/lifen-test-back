#!/usr/bin/env node

'use strict'
const fse = require('fs-extra')
const path = require('path')
const reducePricesByWorker = require('./reducePricesByWorker.js')
const mockedData = require('./data.json')

const json = reducePricesByWorker(mockedData)
const outputFile = path.resolve(__dirname, '../../output/level1/output.json')

async function outputJson(data) {
  try {
    await fse.outputJson(outputFile, data, { spaces: 2 })

    console.log('Successfully wrote to ~/output/level1/output.json')
    console.log(await fse.readJson(outputFile))
  } catch (err) {
    console.error('Error while creating output.json', err)
  }
}

outputJson(json)
