'use strict'

const isWeekendDate = require('../helpers/isWeekendDate')
const sortObjectInArray = require('../helpers/sortObjectInArray')

const priceByStatus = {
  medic: 270,
  intern: 126,
  interim: 480,
}

const fee = { percentPerShift: 0.05, fixedPerInterim: 80 }

function reducePricesByWorker(data) {
  const output = {
    workers: [],
    commission: {
      pdg_fee: 0,
      interim_shifts: 0,
    },
  }

  const result = data.shifts.reduce((acc, currValue) => {
    const userId = currValue.user_id
    const worker = data.workers.find(w => w.id === userId)

    // ignore unknown worker.id
    if (!worker) return acc

    let workerPricePerShift = priceByStatus[worker.status]
    if (isWeekendDate(currValue.start_date)) {
      workerPricePerShift = 2 * workerPricePerShift
    }

    const existingWorkerIndex = acc.workers.findIndex(el => userId === el.id)
    if (existingWorkerIndex === -1) {
      acc.workers.push({ id: userId, price: workerPricePerShift })
    } else {
      const currPrice = acc.workers[existingWorkerIndex].price
      acc.workers[existingWorkerIndex].price = currPrice + workerPricePerShift
    }

    acc.commission.pdg_fee += workerPricePerShift * fee.percentPerShift
    if (worker.status === 'interim') {
      acc.commission.interim_shifts++
      acc.commission.pdg_fee += fee.fixedPerInterim
    }

    return acc
  }, output)
  return Object.assign({}, result, {
    workers: sortObjectInArray({ data: result.workers, key: 'id' }),
  })
}

module.exports = reducePricesByWorker
