#!/usr/bin/env node

'use strict'

// use Node process arguments
const args = process.argv.slice(2)
const level = args[1]

require(`./level${level}/main.js`)
