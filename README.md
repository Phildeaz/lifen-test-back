# Lifen test back

Best practices :
  * TDD working pattern
  * Jest : tests
  * Eslint + Prettier
  * Husky : commit and push hooks

## Prérequisites

* Node > 10.12.0


## Clone project

    git clone git@gitlab.com:Phildeaz/lifen-test-back.git

## Install dependencies

    npm i
    
## Launching tests

    npm run test

## Level X (1 - 4)

(replace X by 1 - 4)

Launch script with:

    node src/levelX/main.js

or

    src/levelX/main.js
    
Output.json is printed on the terminal and you can find the source in ./output/levelX/output.json

EXEMPLE : 

### Level 1

Launch script with:

    node src/level1/main.js

or

    src/level1/main.js
    
Output.json is printed on the terminal and you can find the source in ./output/level1/output.json

## Or use Node process arguments

(replace X by 1 - 4)

Launch script with:

    node src/main.js --level X
    
Output.json is printed on the terminal and you can find the source in ./output/levelX/output.json

### Level 5

Made with Node + Express + Pug
on Heroku free plan service (app server + Postgres db)

    https://lifen-test-level5.herokuapp.com

ps: sorry but I did not put a lot of efforts on UX/UI